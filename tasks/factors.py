def factors(number):
    # ==============
    # Your code here
    factorsList = []
    for div in range(2,number-1):
        if number%div == 0:
            factorsList.append(div)
    return factorsList
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
